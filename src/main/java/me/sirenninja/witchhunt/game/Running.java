package me.sirenninja.witchhunt.game;

import me.sirenninja.witchhunt.WitchHunt;
import me.sirenninja.witchhunt.utils.Scoreboard;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Running implements Runnable{

    @Override
    public void run(){
        if(WitchHunt.getInstance().isGameRunning()){
            int TIME = WitchHunt.getInstance().getMAX_TIME();

            if(TIME <= 0){
                gameOver();
            }

            if(WitchHunt.getInstance().getWitches().size() == 0 || WitchHunt.getInstance().getTownfolks().size() == 0){
                gameOver();
            }

            WitchHunt.getInstance().setMAX_TIME(TIME - 1);
            Scoreboard.updateScoreboard();
        }
    }

    private void gameOver(){
        if(WitchHunt.getInstance().getWitches().size() > WitchHunt.getInstance().getTownfolks().size()){
            // town folks win.
        }else if(WitchHunt.getInstance().getWitches().size() < WitchHunt.getInstance().getTownfolks().size()){
            // witches win.
        }else{
            // draw.
        }

        new BukkitRunnable(){

            @Override
            public void run(){
                WitchHunt.getInstance().resetGame();
                WitchHunt.getInstance().getServer().getOnlinePlayers().forEach(player ->  serverTransfer(player, WitchHunt.getInstance().getReturnServer()));
            }

        }.runTaskLater(WitchHunt.getInstance(), 5*20);
    }

    private void serverTransfer(Player p, String server) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            server = ChatColor.stripColor(server);
            out.writeUTF("Connect");
            out.writeUTF(server);

        } catch (IOException e) {
            e.printStackTrace();
        }
        p.sendPluginMessage(WitchHunt.getInstance(), "BungeeCord", b.toByteArray());
    }
}