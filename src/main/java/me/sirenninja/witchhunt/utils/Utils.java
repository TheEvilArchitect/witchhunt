package me.sirenninja.witchhunt.utils;

import me.sirenninja.witchhunt.WitchHunt;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class Utils {

    public static void sendActionBar(String message) {
        for (Player p : WitchHunt.getInstance().getCurrentlyPlaying())
            sendActionBar(p, message);
    }

    public static void sendActionBar(Player player, String message) {
        player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(getColor(message)));
    }

    public static void sendTitle(String title, String subtitle, int fadeIn, int stay, int fadeOut) {
        for (Player p : WitchHunt.getInstance().getCurrentlyPlaying())
            sendTitle(p, title, subtitle, fadeIn, stay, fadeOut);
    }

    public static void sendTitle(Player player, String title, String subtitle, int fadeIn, int stay, int fadeOut) {
        player.sendTitle(getColor(title), getColor(subtitle), fadeIn, stay, fadeOut);
    }

    public static String getColor(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }

    private static void setArmor(Player player, String side){
        if(WitchHunt.getInstance().getConfig().isSet(side + ".items.armor.head") && isMaterial(WitchHunt.getInstance().getConfig().getString(side + ".items.armor.head")))
            player.getInventory().setHelmet(new ItemStack(Material.getMaterial(WitchHunt.getInstance().getConfig().getString(side + ".items.armor.head").toUpperCase()), 1));

        if(WitchHunt.getInstance().getConfig().isSet(side + ".items.armor.chestplate") && isMaterial(WitchHunt.getInstance().getConfig().getString(side + ".items.armor.chestplate")))
            player.getInventory().setHelmet(new ItemStack(Material.getMaterial(WitchHunt.getInstance().getConfig().getString(side + ".items.armor.chestplate").toUpperCase()), 1));

        if(WitchHunt.getInstance().getConfig().isSet(side + ".items.armor.leggings") && isMaterial(WitchHunt.getInstance().getConfig().getString(side + ".items.armor.leggings")))
            player.getInventory().setHelmet(new ItemStack(Material.getMaterial(WitchHunt.getInstance().getConfig().getString(side + ".items.armor.leggings").toUpperCase()), 1));

        if(WitchHunt.getInstance().getConfig().isSet(side + ".items.armor.boots") && isMaterial(WitchHunt.getInstance().getConfig().getString(side + ".items.armor.boots")))
            player.getInventory().setHelmet(new ItemStack(Material.getMaterial(WitchHunt.getInstance().getConfig().getString(side + ".items.armor.boots").toUpperCase()), 1));
    }

    public static void setContents(Player player, String side){
        setArmor(player, side);

        for(String con : WitchHunt.getInstance().getConfig().getConfigurationSection(side + ".items").getKeys(false)){
            if(isNumber(con)){
                int slot = Integer.parseInt(con);

                String[] pt = WitchHunt.getInstance().getConfig().getString(side + "." + con + ".itemname").split(":");
                int amount = getAmount(WitchHunt.getInstance().getConfig().getString(side + "." + con + ".amount"));
                boolean splash = isSplash(WitchHunt.getInstance().getConfig().getString(side + "." + con + ".splash"));
                String name = getName(WitchHunt.getInstance().getConfig().getString(side + "." + con + ".displayname"));

                String enc = WitchHunt.getInstance().getConfig().getString(side + "." + con + ".enchantments");

                if(isPotion(WitchHunt.getInstance().getConfig().getString(side + "." + con + ".itemname"))){
                    player.getInventory().setItem(slot, getPotion(pt[1], amount, splash, name));
                }else{
                    if(WitchHunt.getInstance().getConfig().isSet(side + "." + con + ".itemname")){
                        if(isMaterial(WitchHunt.getInstance().getConfig().getString(side + "." + con + ".itemname"))){
                            player.getInventory().setItem(slot, getItem(enc, WitchHunt.getInstance().getConfig().getString(side + "." + con + ".itemname"), amount, name));
                        }
                    }
                }
            }
        }
    }

    public static void teleportTF(){
        ArrayList<Location> tempLocs = WitchHunt.getInstance().getTownFolkLocations();

        for(Player p : WitchHunt.getInstance().getTownfolks()){
            for(int i = 0; i < tempLocs.size(); i++){
                int r = randomNumber(tempLocs.size());

                p.teleport(tempLocs.get(r));
                tempLocs.remove(r);
            }
        }
    }

    public static void teleportW(){
        ArrayList<Location> tempLocs = WitchHunt.getInstance().getWitchLocations();

        for(Player p : WitchHunt.getInstance().getWitches()){
            for(int i = 0; i < tempLocs.size(); i++){
                int r = randomNumber(tempLocs.size());

                p.teleport(tempLocs.get(r));
                tempLocs.remove(r);
            }
        }
    }

    private static boolean isMaterial(String material){
        return Material.getMaterial(material.toUpperCase()) != null;
    }

    public static int randomNumber(int size){
        return new Random().nextInt(size);
    }

    private static boolean isNumber(String s){
        return s.matches("\\d+");
    }

    private static boolean isPotion(String item){
        return item.startsWith("POTION:");
    }

    private static int getAmount(String item){
        if(WitchHunt.getInstance().getConfig().isSet(item)){
            return WitchHunt.getInstance().getConfig().getInt(item);
        }

        return 1;
    }

    private static boolean isSplash(String item){
        return WitchHunt.getInstance().getConfig().isSet(item) && WitchHunt.getInstance().getConfig().getBoolean(item);
    }

    private static String getName(String item){
        if(WitchHunt.getInstance().getConfig().isSet(item))
            return WitchHunt.getInstance().getConfig().getString(Utils.getColor(item));

        return null;
    }

    private static ItemStack getPotion(String type, int amount, boolean splash, String name){
        ItemStack potion;

        if(splash)
            potion = new ItemStack(Material.SPLASH_POTION, amount);
        else
            potion = new ItemStack(Material.POTION, amount);

        PotionMeta meta = (PotionMeta) potion.getItemMeta();
        meta.setBasePotionData(new PotionData(PotionType.valueOf(type.toUpperCase()), false, false));

        if(!(name == null))
            meta.setDisplayName(name);

        potion.setItemMeta(meta);

        return potion;
    }

    private static ItemStack getItem(String enc, String type, int amount, String name){
        HashMap<Enchantment, Integer> enchants = new HashMap<>();

        if(WitchHunt.getInstance().getConfig().isSet(enc)){
            for(String s : WitchHunt.getInstance().getConfig().getStringList(enc)) {
                String[] en = s.split(":");
                enchants.put(Enchantment.getByName(en[0].toUpperCase()), (isNumber(en[1]) ? Integer.parseInt(en[1]) : 0));
            }
        }

        ItemStack i = new ItemStack(Material.getMaterial(type.toUpperCase()));
        ItemMeta meta = i.getItemMeta();

        if(!(name == null))
            meta.setDisplayName(name);

        if(enchants.size() > 0)
            i.addEnchantments(enchants);

        i.setItemMeta(meta);
        return i;
    }
}