package me.sirenninja.witchhunt;

import me.sirenninja.witchhunt.events.*;
import me.sirenninja.witchhunt.game.*;
import me.sirenninja.witchhunt.utils.Scoreboard;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;

public class WitchHunt extends JavaPlugin {

    /**
     * TODO:
     * Create the commands to create/remove arenas and such.
     * Fully transfer from the bungeecord type minigame to an arena one. (Running, Starting runnables, and events).
     * Fix some messages.
     * Then do nothing with the plugin, because it'll probably never be released.
     */

    private static WitchHunt instance;

    private String returnServer;

    private ArrayList<Player> currentlyPlaying = new ArrayList<>();

    private ArrayList<Player> witches = new ArrayList<>();
    private ArrayList<Player> townfolks = new ArrayList<>();

    private ArrayList<Player> deadPlayers = new ArrayList<>();

    private int TF_MAXSIZE = 0;
    private int W_MAXSIZE = 0;

    private boolean IS_SPECTATOR_ALLOWED = false;
    private int MAX_SPECTATORS = 0;

    private int MIN_PLAYERS = 0;
    private int MAX_PLAYERS = 0;

    private int MAX_TIME = 0;

    private int startingCountdown = 45;

    private boolean isGameRunning = false;

    @Override
    public void onEnable() {
        instance = this;

        getConfig().options().copyDefaults(true);
        saveDefaultConfig();

        Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

        if(!(getConfig().isSet("witches.spawnpoints")) || !(getConfig().isSet("townfolks.spawnpoints"))){
            // TODO: Set up log to say spawnpoints need to be set up.
            return;
        }

        getServer().getPluginManager().registerEvents(new PlayerEvents(), this);

        getServer().getScheduler().runTaskTimerAsynchronously(this, new Starting(), 0, 20);
        getServer().getScheduler().runTaskTimerAsynchronously(this, new Running(), 0, 20);

        setupGame();

        if(getIS_SPECTATOR_ALLOWED()){
            if(getConfig().isSet("global.spectator.maxspectators")){
                setMAX_SPECTATORS(getConfig().getInt("global.spectator.maxspectators"));
            }else{
                setIS_SPECTATOR_ALLOWED(false);
            }
        }
    }

    @Override
    public void onDisable() {
        Bukkit.getMessenger().unregisterOutgoingPluginChannel(this, "BungeeCord");
        resetGame();
    }

    private void setupGame(){
        currentlyPlaying.addAll(getServer().getOnlinePlayers());

        setReturnServer(getConfig().getString("global.returnserver"));

        setMIN_PLAYERS(getConfig().getInt("global.minplayers"));
        setMAX_PLAYERS(getConfig().getInt("global.maxplayers"));

        setW_MAXSIZE(getConfig().getInt("witches.maxsize"));
        setTF_MAXSIZE(W_MAXSIZE-MAX_PLAYERS);

        setMAX_TIME(getConfig().getInt("global.max_time_in_seconds"));

        setIS_SPECTATOR_ALLOWED(getConfig().getBoolean("global.spectator.enabled"));
    }

    public void resetGame(){
        currentlyPlaying.clear();
        witches.clear();
        townfolks.clear();
        currentlyPlaying.clear();
        deadPlayers.clear();
        setGameRunning(false);
        setStartingCountdown(45);
        setupGame();
    }

    public static WitchHunt getInstance(){
        return instance;
    }

    public String getReturnServer() {
        return returnServer;
    }

    public void setReturnServer(String server){
        returnServer = server;
    }

    public ArrayList<Player> getCurrentlyPlaying() {
        return currentlyPlaying;
    }

    public void addCurrentlyPlaying(Player player) {
        currentlyPlaying.add(player);
    }

    public void removeCurrentlyPlaying(Player player) {
        currentlyPlaying.remove(player);
    }

    public ArrayList<Player> getWitches() {
        return witches;
    }

    public void addWitches(Player player) {
        witches.add(player);
    }

    public void removeWitches(Player player) {
        witches.remove(player);
    }

    public ArrayList<Player> getTownfolks() {
        return townfolks;
    }

    public void addTownfolks(Player player) {
        townfolks.add(player);
    }

    public void removeTownfolks(Player player) {
        townfolks.remove(player);
    }

    public ArrayList<Player> getDeadPlayers() {
        return deadPlayers;
    }

    public void addDeadPlayers(Player player) {
        deadPlayers.add(player);
    }

    public void removeDeadPlayers(Player player) {
        deadPlayers.remove(player);
    }

    public int getTF_MAXSIZE() {
        return TF_MAXSIZE;
    }

    private void setTF_MAXSIZE(int tF_MAXSIZE) {
        TF_MAXSIZE = tF_MAXSIZE;
    }

    public int getW_MAXSIZE() {
        return W_MAXSIZE;
    }

    private void setW_MAXSIZE(int w_MAXSIZE) {
        W_MAXSIZE = w_MAXSIZE;
    }

    public int getMIN_PLAYERS() {
        return MIN_PLAYERS;
    }

    private void setMIN_PLAYERS(int mIN_PLAYERS) {
        MIN_PLAYERS = mIN_PLAYERS;
    }

    public int getMAX_PLAYERS() {
        return MAX_PLAYERS;
    }

    private void setMAX_PLAYERS(int mAX_PLAYERS) {
        MAX_PLAYERS = mAX_PLAYERS;
    }

    public boolean getIS_SPECTATOR_ALLOWED() {
        return IS_SPECTATOR_ALLOWED;
    }

    public void setIS_SPECTATOR_ALLOWED(boolean state) {
        IS_SPECTATOR_ALLOWED = state;
    }

    public int getMAX_TIME() {
        return MAX_TIME;
    }

    public void setMAX_TIME(int mAX_TIME) {
        MAX_TIME = mAX_TIME;
    }

    public int getMAX_SPECTATORS() {
        return MAX_SPECTATORS;
    }

    public void setMAX_SPECTATORS(int mAX_SPECTATORS) {
        MAX_SPECTATORS = mAX_SPECTATORS;
    }

    public boolean isGameRunning() {
        return isGameRunning;
    }

    public void setGameRunning(boolean isGameRunning) {
        this.isGameRunning = isGameRunning;
    }

    public int getStartingCountdown() {
        return startingCountdown;
    }

    public void setStartingCountdown(int startingCountdown) {
        this.startingCountdown = startingCountdown;
    }
}
