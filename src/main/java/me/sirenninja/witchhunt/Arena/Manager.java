package me.sirenninja.witchhunt.Arena;

import me.sirenninja.witchhunt.WitchHunt;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.*;

public class Manager {

    private static Manager arenaManager = new Manager();

    private Map<UUID, Location> locs = new HashMap<>();

    private List<Arena> arenas = new ArrayList<>();
    private int arenaSize = 0;

    public static Manager getAM(){
        return arenaManager;
    }

    public Manager(){

    }

    public Arena getArena(int arenaID){
        for(Arena arena : arenas){
            if(arena.getId() == arenaID)
                return arena;

        }

        return null;
    }

    public Arena reloadArena(Location location){
        int num = arenaSize+1;
        arenaSize++;

        Arena arena = new Arena(location, num);
        arenas.add(arena);

        return arena;
    }

    public Arena createArena(Location location){
        int num = arenaSize + 1;
        arenaSize++;

        Arena arena = new Arena(location, num);
        arenas.add(arena);

        WitchHunt.getInstance().getConfig().set("Arenas." + num, setCorrectLocation(location));

        List<Integer> list = WitchHunt.getInstance().getConfig().getIntegerList("Arenas.Arenas");
        list.add(num);

        WitchHunt.getInstance().getConfig().set("Arenas.Arenas", list);
        WitchHunt.getInstance().saveConfig();

        return arena;
    }

    public void removeArena(int arenaID){
        Arena arena = getArena(arenaID);
        if(arena == null)
            return;

        arenas.remove(arena);

        WitchHunt.getInstance().getConfig().set("Arenas." + arenaID, null);

        List<Integer> list = WitchHunt.getInstance().getConfig().getIntegerList("Arenas.Arenas");
        list.remove(arenaID);

        WitchHunt.getInstance().getConfig().set("Arenas.Arenas", list);
        WitchHunt.getInstance().saveConfig();
    }

    public void addPlayer(Player player, int arenaID){
        Arena arena = getArena(arenaID);

        if(arena == null)
            return;

        arena.getPlayers().add(player.getUniqueId());

        clearInventory(player);

        locs.put(player.getUniqueId(), player.getLocation());
        player.teleport(arena.getSpawn());

    }

    public void removePlayer(Player player, boolean died){
        Arena a = null;

        for(Arena arena : arenas){
            if(arena.getPlayers().contains(player.getUniqueId()))
                a = arena;
        }

        if((a == null) || (!a.getPlayers().contains(player.getUniqueId())))
            return;

        a.getPlayers().remove(player.getUniqueId());

        if(died) {
            player.setGameMode(GameMode.SPECTATOR);
            player.teleport(a.getSpectatorSpawn());
        }else {
            player.teleport(locs.get(player.getUniqueId()));
        }

        locs.remove(player.getUniqueId());
    }

    public boolean isInGame(Player player){
        for(Arena arena : arenas){
            if(arena.getPlayers().contains(player.getUniqueId()))
                return true;
        }

        return false;
    }

    public void loadGames(){
        arenaSize = 0;

        if(WitchHunt.getInstance().getConfig().getIntegerList("Arenas.Arenas").isEmpty())
            return;

        for(int i : WitchHunt.getInstance().getConfig().getIntegerList("Arenas.Arenas")){
            Arena arena = reloadArena(getLocation(WitchHunt.getInstance().getConfig().getString("Arenas." + i)));

            arena.setId(i);
        }
    }

    private String setCorrectLocation(Location location){
        return location.getWorld().getName() + "," + location.getBlockX() + "," + location.getBlockY() + "," + location.getBlockZ();
    }

    private Location getLocation(String location){
        String[] loc = location.split(",");

        return new Location(Bukkit.getWorld(loc[0]), Integer.parseInt(loc[1]), Integer.parseInt(loc[2]), Integer.parseInt(loc[3]));
    }

    private void clearInventory(Player player){
        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
    }
}