package me.sirenninja.witchhunt.game;

import me.sirenninja.witchhunt.WitchHunt;
import me.sirenninja.witchhunt.utils.*;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;

public class Starting implements Runnable {

    private Integer[] count = {45, 30, 20, 10, 5};

    @Override
    public void run() {
        if (!(WitchHunt.getInstance().isGameRunning())) {
            int CP = WitchHunt.getInstance().getCurrentlyPlaying().size();
            int MIN = WitchHunt.getInstance().getMIN_PLAYERS();
            int TIME = WitchHunt.getInstance().getStartingCountdown();

            if (CP < MIN) {
                Utils.sendActionBar("&cWAITING FOR " + ((CP - MIN) == 1 ? "1 MORE PLAYER!" : (CP - MIN) + " MORE PLAYERS!"));

                if(TIME < count[0])
                    WitchHunt.getInstance().resetGame();

                return;
            }

            if (arrayContains(count, TIME))
                Utils.sendTitle("&c&l" + TIME + (TIME == 1 ? " &cSECOND" : " &cSECONDS") + " LEFT", "&cUNTIL GAME STARTS!", 1, 1, 1);

            if (TIME <= 0) {
                ArrayList<Player> tempPlayers = WitchHunt.getInstance().getCurrentlyPlaying();

                for(int i = 1; i <= WitchHunt.getInstance().getW_MAXSIZE(); i++){
                    if(i <= WitchHunt.getInstance().getW_MAXSIZE()){
                        int r = Utils.randomNumber(tempPlayers.size());

                        WitchHunt.getInstance().addWitches(tempPlayers.get(r));
                        tempPlayers.remove(r);
                    }
                }

                for(Player tf : tempPlayers)
                    WitchHunt.getInstance().addTownfolks(tf);

                tempPlayers.clear();

                Utils.teleportTF();
                Utils.teleportW();

                for(Player p : WitchHunt.getInstance().getCurrentlyPlaying()){
                    if(WitchHunt.getInstance().getTownfolks().contains(p)) {
                        Utils.setContents(p, "townfolks");

                    } else if(WitchHunt.getInstance().getWitches().contains(p)) {
                        Utils.setContents(p, "witches");
                    }
                }

                Scoreboard.updateScoreboard();
                WitchHunt.getInstance().setGameRunning(true);
            }

            WitchHunt.getInstance().setStartingCountdown(TIME - 1);
        }
    }

    private boolean arrayContains(Integer[] array, int value) {
        return value < count[count.length - 1] && value > 0 || Arrays.asList(array).contains(value);
    }
}