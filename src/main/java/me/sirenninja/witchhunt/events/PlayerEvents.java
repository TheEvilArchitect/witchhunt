package me.sirenninja.witchhunt.events;

import me.sirenninja.witchhunt.WitchHunt;
import me.sirenninja.witchhunt.utils.Utils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerEvents implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event){
        event.setJoinMessage(null);
        event.getPlayer().setHealth(event.getPlayer().getMaxHealth());
        event.getPlayer().setFoodLevel(20);

        if(WitchHunt.getInstance().isGameRunning()){
            WitchHunt.getInstance().addDeadPlayers(event.getPlayer());

            WitchHunt.getInstance().getServer().broadcastMessage(Utils.getColor("&c&lWitchHunt &7>> &c" + event.getPlayer().getName() + " &7has joined as a &3Spectator&7!"));
            return;
        }

        WitchHunt.getInstance().addCurrentlyPlaying(event.getPlayer());
        int MAX_PLAYERS = WitchHunt.getInstance().getMAX_PLAYERS();
        int CP = WitchHunt.getInstance().getCurrentlyPlaying().size();

        WitchHunt.getInstance().getServer().broadcastMessage(Utils.getColor("&c&lWitchHunt &7>> &2" + event.getPlayer().getName() + " &ahas joined! (&c" + CP + "&a/&c" + MAX_PLAYERS + "&a)"));

    }

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent event){
        WitchHunt.getInstance().removePlayer(event.getPlayer(), false);

        int MAX_PLAYERS = WitchHunt.getInstance().getMAX_PLAYERS();
        int CP = WitchHunt.getInstance().getCurrentlyPlaying().size();

        WitchHunt.getInstance().getServer().broadcastMessage(Utils.getColor("&c&lWitchHunt &7>> &c" + event.getPlayer().getName() + " &chas left! (&7" + CP + "&c/&7" + MAX_PLAYERS + "&c)"));
    }

    @EventHandler
    public void onDamage(EntityDamageEvent event){
        if(!(WitchHunt.getInstance().isGameRunning()))
            event.setCancelled(true);

        if(!(event.getEntity() instanceof Player))
            return;

        Player player = (Player)event.getEntity();

        if(player.getHealth()-event.getDamage() <= 0) {
            event.setCancelled(true);

            WitchHunt.getInstance().removePlayer(player, true);
        }
    }

    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent event){
        event.setCancelled(true);
    }
}