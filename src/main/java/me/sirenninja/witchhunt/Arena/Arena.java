package me.sirenninja.witchhunt.Arena;

import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Arena {

    private int id;
    private Location spawnLocation;
    private Location spectatorLocation;
    private Location townFolkLocation;
    private Location witchLocation;

    private List<UUID> players = new ArrayList<>();

    public Arena(Location location, int id){
        this.id = id;
        this.spawnLocation = location;
    }

    public Arena(Location location, Location spec, Location town, Location witch, int id){
        this.id = id;
        this.spawnLocation = location;
        this.spectatorLocation = spec;
        this.townFolkLocation = town;
        this.witchLocation = witch;
    }

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public List<UUID> getPlayers(){
        return players;
    }

    public Location getSpawn(){
        return spawnLocation;
    }

    public Location getSpectatorSpawn(){
        return spectatorLocation;
    }

    public Location getTownFolkLocation(){
        return townFolkLocation;
    }

    public Location getWitchLocation(){
        return witchLocation;
    }
}
