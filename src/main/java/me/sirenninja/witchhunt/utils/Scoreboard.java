package me.sirenninja.witchhunt.utils;

import me.sirenninja.witchhunt.WitchHunt;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;

public class Scoreboard {

    public static void updateScoreboard() {
        new BukkitRunnable() {

            @Override
            public void run() {
                setScoreboard();
            }

        }.runTaskLater(WitchHunt.getInstance(), 5);
    }

    private static void setScoreboard() {
        org.bukkit.scoreboard.Scoreboard board = WitchHunt.getInstance().getServer().getScoreboardManager().getNewScoreboard();
        Objective objective = board.registerNewObjective("Scoreboard", "dummy");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);

        objective.setDisplayName(ChatColor.GRAY + "WITCH" + ChatColor.RED + " HUNT");

        String[] lines = new String[]{
                "",
                ChatColor.GRAY + "Town Folks: ",
                ChatColor.RED + "" + WitchHunt.getInstance().getTownfolks().size(),
                " ",
                ChatColor.GRAY + "Witches: ",
                ChatColor.RED + "" + WitchHunt.getInstance().getWitches().size() + ChatColor.RESET,
                "  ",
                ChatColor.GRAY + "Time Left: ",
                ChatColor.RED + "" + (WitchHunt.getInstance().getMAX_TIME() / 60) + ChatColor.GRAY + "min " + ChatColor.RED + (WitchHunt.getInstance().getMAX_TIME() % 60) + ChatColor.GRAY + "sec",
                "   "
        };

        for (int i = 0; i < lines.length; i++) {
            Score score = objective.getScore(lines[i]);
            score.setScore(lines.length - i);
        }

        for(Player p : WitchHunt.getInstance().getCurrentlyPlaying())
            p.setScoreboard(board);
    }
}